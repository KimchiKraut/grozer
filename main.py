from datetime import datetime, timedelta
import json
import os
import random
import sqlite3

import bottle
import pytz


PRACTICE_LIMIT = 10
MIN_FREQUENCY = 5

DATA_PATH = os.environ.get('DATA_PATH', '.')


def main():
    print('Starting up')
    bottle.run(host='0.0.0.0', port=3000, debug=True)


@bottle.route('/')
def dashboard_page():
    with open_db() as db:
        summary = count_cards(db)
        word_stats = get_word_stats(db)

        return template(
            '''
            Cards: {{total_cards}} total; {{new_cards}} new; {{review_cards}} to review.<br>
            Words: {{total_words}} total; {{carded_words}} in cards; {{active_words}} active; {{inactive_words}} inactive.<br>
            <br>
            <a href="/texts">Edit texts</a><br>
            <br>
            <a href="/new">Create new</a><br>
            <br>
            <a href="/practice/review">Practice review</a><br>
            ''',
            total_cards=summary['total'],
            new_cards=summary['new'],
            review_cards=summary['review'],
            total_words=word_stats['total'],
            carded_words=word_stats['carded'],
            active_words=word_stats['active'],
            inactive_words=word_stats['inactive'],
        )


@bottle.route('/new')
def create_new_page():
    with open_db() as db:
        form = generate_next_form(db)
        return bottle.redirect('/new/' + form)


@bottle.route('/new/<form>')
def create_new_page(form):
    with open_db() as db:
        potentials = generate_new_potentials(db, form, 20)
        counts = count_cards(db)
        word_stats = get_word_stats(db)

        examples = [
            template(
                '''
                <p>
                    {{sentence}}<br>
                    {{translation}}
                    <label>
                        <input type="checkbox" name="sentences" value="{{id}}">
                        Include
                    </label>
                </p>
                ''',
                id=potential['sentence_id'],
                sentence=potential['text'],
                translation=potential['translation'])
            for potential in potentials]

        return template(
            '''
            <h1>{{word}}</h1>
            <p>
                <a href="/">Dashboard</a>
                {{new_count}} new cards
                {{new_words}} new words
                {{available_words}} available words
            </p>
            <h2>Examples</h2>
            <form method="post" action="/new/cards">
                <input type="hidden" name="form" value="{{word}}">
                {{!examples}}
                <input type="submit" value="Create cards">
            </form>
            ''',
            word=form,
            new_count=counts['new'],
            new_words=word_stats['new'],
            available_words=word_stats['active'] - word_stats['carded'],
            examples='\n'.join(examples),
        )


@bottle.route('/new/cards', method='post')
def create_new_cards_handler():
    params = bottle.request.forms.decode()
    form = params.get('form')
    sentence_ids = params.getall('sentences')
    with open_db() as db:
        for id in sentence_ids:
            id = int(id)
            create_card(db, id, form)
        return bottle.redirect('/new')


@bottle.route('/texts')
def texts_page():
    with open_db() as db:
        texts = get_texts(db)

        for text in texts:
            text['known_percent'] = text['carded_words'] * 100. / text['word_count']
        texts.sort(key=lambda t: (t['enabled'], t['known_percent']), reverse=True)

        texts_html = [
            template(
                '''
                <p>
                    {{title}}
                    <br>

                    {{sentence_count}} sentences,
                    {{carded_words}}/{{word_count}},
                    {{known_percent}}% words in cards

                    <label>
                        <input type="checkbox" name="texts" value="{{id}}" {{checked}}>
                        Enabled
                    </label>
                </p>
                ''',
                id=text['id'],
                title=text['title'],
                word_count=text['word_count'],
                carded_words=text['carded_words'],
                sentence_count=text['sentence_count'],
                known_percent='%.1f' % text['known_percent'],
                checked='checked' if text['enabled'] else '',
            ) for text in texts]

        return template(
            '''
            <h1>Texts</h1>
            <p>
                <a href="/">Dashboard</a>
            </p>
            <h2>Examples</h2>
            <form method="post" action="/texts/edit">
                {{!body}}
                <input type="submit" value="Update texts">
            </form>
            ''',
            body='\n'.join(texts_html),
        )


@bottle.route('/texts/edit', method='post')
def update_texts_handler():
    params = bottle.request.forms.decode()
    enabled_ids = params.getall('texts')
    with open_db() as db:
        enable_only_texts(db, enabled_ids)
        return bottle.redirect('/texts')


@bottle.route('/practice/review')
def practice_review_page():
    with open_db() as db:
        to_go = get_review_cards(db, PRACTICE_LIMIT)
        practices = [get_practice(db, id) for id in to_go]
        return practice_page_template(practices)


def practice_page_template(practices):
    data = {
        'practice': practices
    }

    return template(
        '''
        <div class="card">
            <div class="stats"></div>
            <div class="title"></div>
            <div class="translation"></div>
            <div class="context">
                <span class="left"></span>
                <span class="text"></span>
                <span class="right"></span>
            </div>
            <div class="choices">
                <button class="c1"></button>
                <button class="c2"></button>
                <button class="c3"></button>
                <button class="c4"></button>
            </div>

            <button class="next">Next</button>
        </div>

        <script type="application/javascript">
            window.grozerData = {{!data}}
        </script>

        <script src="/session.js"></script>
        <link rel="stylesheet" href="/session.css" type="text/css" media="all">
        ''',
        data=json.dumps(data),
    )


@bottle.route('/session.js')
def asset_session_js():
    return bottle.static_file('/session.js',
                              root=os.path.dirname(__file__),
                              mimetype='application/javascript')

@bottle.route('/session.css')
def asset_session_css():
    return bottle.static_file('/session.css',
                              root=os.path.dirname(__file__),
                              mimetype='text/css')


@bottle.route('/api/card/<id:int>/<correct>', method='post')
def update_practice_route(id, correct):
    with open_db() as db:
        card = get_card(db, id)
        update_card_review(db, card, correct=(correct == 'true'))


def template(content, **kwargs):
    content = bottle.template(content, **kwargs)
    return bottle.template(
        '''
        <!doctype html>
        <html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Grozer</title>
        </head>
        <body>
            {{!content}}
        </body>
        </html>
        '''.strip(), content=content)


def load_ignore_list():
    list_path = os.path.join(DATA_PATH, 'ignore.txt')
    with open(list_path, 'r') as fp:
        return [word
                for word in (w.strip() for w in fp)
                if word]


def open_db():
    db_path = os.path.join(DATA_PATH, 'main.db')
    db = sqlite3.connect(db_path)
    try:
        db.execute(
            '''
            CREATE TABLE IF NOT EXISTS card (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                -- set created_at to "now", in UTC
                created_at TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%S', 'now')),
                sentence_id INT,
                word TEXT,
                cloze_text TEXT,
                text TEXT,
                translation TEXT,
                review_period_days INT,
                next_review TEXT
            );
            ''')

        db.execute('''CREATE INDEX IF NOT EXISTS card_sentence_id ON card (sentence_id);''')
        db.execute('''CREATE INDEX IF NOT EXISTS card_word ON card (word);''')
        db.execute('''CREATE INDEX IF NOT EXISTS card_next_review ON card (next_review);''')

        db.execute(
            '''
            CREATE TABLE IF NOT EXISTS review (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                -- set created_at to "now", in UTC
                reviewed_at TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%S', 'now')),
                card_id INTEGER,
                was_correct BOOLEAN
            );
            ''')

        db.execute('''CREATE INDEX IF NOT EXISTS review_card_id ON review (card_id);''')
        db.execute('''CREATE INDEX IF NOT EXISTS review_reviewed_at ON review (reviewed_at);''')


        # insert the system ignored words into a temporary table
        db.execute(
            '''
            CREATE TEMP TABLE system_ignored_form (
                id INTEGER PRIMARY KEY,
                form TEXT
            );
            ''')
        db.executemany(
            '''INSERT INTO system_ignored_form (form) VALUES (?);''',
            [(w,) for w in load_ignore_list()])

        # a view to get known forms
        db.execute(
            '''
            CREATE TEMP VIEW known_form (form) AS
            SELECT form FROM system_ignored_form UNION
            SELECT word FROM card;
            ''')

        # a view to get list all learnable words (both known and unknown),
        db.execute(
            f'''
            CREATE TEMP VIEW learnable_form AS
            SELECT form FROM form
            WHERE frequency >= {int(MIN_FREQUENCY)};
            ''')

        # a view of sentences to known word counts
        db.execute(
            '''
            CREATE TEMP VIEW sentence_known_words AS
            SELECT sentence_id AS id,
                   COUNT(*) as known_words
            FROM word
            WHERE form IN (SELECT form FROM known_form)
            GROUP BY sentence_id;
            ''')

        # a view of sentences to unknown word counts
        db.execute(
            '''
            CREATE TEMP VIEW sentence_unknown_words AS
            SELECT sentence_id AS id,
                   COUNT(*) as unknown_words
            FROM word
            WHERE form IN (SELECT form FROM learnable_form)
              AND form NOT IN (SELECT form FROM known_form)
            GROUP BY sentence_id;
            ''')

        # a view of sentences with stats
        db.execute(
            '''
            CREATE TEMP VIEW sentence_stats AS
            SELECT id, word_count, known_words, unknown_words,
                   length,
                   (known_words * 1.0) / word_count AS known_fraction
            FROM (
                SELECT sentence.id,
                       (end_word - start_word + 1) AS word_count,
                       COALESCE(known_words, 0) AS known_words,
                       COALESCE(unknown_words, 0) AS unknown_words,
                       length(text) AS length
                FROM sentence
                LEFT JOIN sentence_known_words
                       ON sentence.id = sentence_known_words.id
                LEFT JOIN sentence_unknown_words
                       ON sentence.id = sentence_unknown_words.id);
            ''')

        db.commit()
        return db
    except Exception as e:
        db.close()
        raise e


def count_cards(db):
    c = db.execute(
        '''
        SELECT COUNT(*) FROM card
        WHERE next_review IS NULL
          AND id NOT IN (SELECT card_id from review);
        ''')
    new_cards = c.fetchone()[0]

    c = db.execute(
        '''
        SELECT COUNT(*) FROM card
        WHERE next_review <= strftime('%Y-%m-%dT%H:%M:%S', 'now');
        ''')
    review_cards = c.fetchone()[0]

    c = db.execute(
        '''
        SELECT COUNT(*) FROM card;
        ''')
    total_cards = c.fetchone()[0]

    return {'new': new_cards, 'review': review_cards, 'total': total_cards}


def get_word_stats(db):
    c = db.execute(
        'SELECT COUNT(*) FROM form WHERE frequency >= ?;',
        (MIN_FREQUENCY,))
    forms = c.fetchone()[0]

    c = db.execute('SELECT COUNT(DISTINCT word) FROM card;')
    carded = c.fetchone()[0]

    c = db.execute(
        '''
        SELECT COUNT(*)
        FROM form
        WHERE form IN (SELECT form
                       FROM word
                       JOIN text ON text.id = word.text_id
                                AND text.enabled = 1)
          AND frequency >= ?;
        ''',
        (MIN_FREQUENCY,))
    active = c.fetchone()[0]

    c = db.execute(
        '''
        SELECT COUNT(DISTINCT word) FROM card
        WHERE next_review IS NULL;
        ''')
    new_count = c.fetchone()[0]

    return {'total': forms,
            'carded': carded,
            'active': active,
            'inactive': forms - active,
            'new': new_count}


def get_texts(db, limit=1_000_000):
    c = db.execute(
        '''
        SELECT id, title, enabled,
               sentence_count.count,
               word_count.count,
               carded_count.count
        FROM text
        LEFT JOIN (SELECT text_id, COUNT(*) AS count
                   FROM sentence
                   GROUP BY text_id)
             AS sentence_count
             ON sentence_count.text_id = text.id
        LEFT JOIN (SELECT text_id, COUNT(DISTINCT form) AS count
                   FROM word
                   WHERE form IN (SELECT form FROM form WHERE frequency >= ?)
                   GROUP BY text_id)
             AS word_count
             ON word_count.text_id = text.id
        LEFT JOIN (SELECT text_id, COUNT(DISTINCT form) AS count
                   FROM word
                   WHERE form IN (SELECT word FROM card)
                   GROUP BY text_id)
             AS carded_count
             ON carded_count.text_id = text.id
        LIMIT ?;
        ''',
        (MIN_FREQUENCY, limit))
    return [{
        'id': r[0],
        'title': r[1],
        'enabled': r[2] > 0,
        'sentence_count': r[3],
        'word_count': r[4],
        'carded_words': r[5],
    } for r in c]


def get_review_cards(db, limit):
    c = db.execute(
        '''
        SELECT id FROM card
        WHERE next_review <= strftime('%Y-%m-%dT%H:%M:%S', 'now')
           OR next_review IS NULL
        ORDER BY review_period_days ASC,
                 RANDOM()
        LIMIT ?;
        ''',
        (limit,))
    return [r[0] for r in c]


def get_card(db, id):
    c = db.execute(
        '''
        SELECT id, created_at, sentence_id,
               word, cloze_text, text, translation,
               review_period_days, next_review
        FROM card
        WHERE id = ?;
        ''',
        (id,))
    row = c.fetchone()
    return {'id': row[0], 'created_at': row[1], 'sentence_id': row[2],
            'word': row[3], 'cloze_text': row[4], 'text': row[5],
            'translation': row[6],
            'review_period_days': row[7], 'next_review': row[8]}


def get_practice(db, card_id):
    ignore_list = load_ignore_list()

    card = get_card(db, card_id)

    # get form number
    c = db.execute(
        '''
        SELECT id FROM form
        WHERE form = ?;
        ''',
        (card['word'],))
    form_id = c.fetchone()[0]

    # get random forms nearby in frequency list
    frequency_range = 10
    number_of_choices = 4
    c = db.execute(
        f'''
        SELECT form
        FROM (
            SELECT form FROM form
            WHERE id != ?
              AND form NOT IN ({', '.join(['?'] * len(ignore_list))})
              AND form IN (SELECT word from card)
            ORDER BY abs(id - ?)
            LIMIT ?)
        ORDER BY RANDOM()
        LIMIT ?;
        ''',
        (form_id,
         *ignore_list,
         form_id,
         frequency_range,
         number_of_choices - 1))
    choices = [r[0] for r in c]
    choices.append(card['word'])
    random.shuffle(choices)

    # get left-hand context for sentence
    c = db.execute(
        '''
        SELECT text FROM sentence
        WHERE id = ? - 1
          AND book IN (SELECT book FROM sentence
                       WHERE id = ?);
        ''',
        (card['sentence_id'], card['sentence_id']))
    left_context = c.fetchone()
    if left_context:
        left_context = left_context[0]

    # get right-hand context for sentence
    c = db.execute(
        '''
        SELECT text FROM sentence
        WHERE id = ? + 1
          AND book IN (SELECT book FROM sentence
                       WHERE id = ?);
        ''',
        (card['sentence_id'], card['sentence_id']))
    right_context = c.fetchone()
    if right_context:
        right_context = right_context[0]

    return {'id': card_id,
            'word': card['word'],
            'cloze_text': card['cloze_text'],
            'text': card['text'],
            'translation': card['translation'],
            'left_context': left_context,
            'right_context': right_context,
            'choices': choices,
            }


def generate_next_form(db, shuffle_range=10):
    # pick random from Sentences with Least words to Learn
    c = db.execute(
        '''
        SELECT *
        FROM (SELECT sentence.id FROM sentence_stats
              JOIN sentence ON sentence.id = sentence_stats.id
              WHERE unknown_words >= 1
                AND sentence.text_id IN (SELECT id from text WHERE enabled = 1)
              ORDER BY unknown_words ASC,
                       length ASC
              LIMIT :shuffle_range)
        ORDER BY RANDOM()
        LIMIT 1;
        ''',
        {'shuffle_range': shuffle_range})
    sentence_id = c.fetchone()[0]

    # pick most freq word from that sentence
    c = db.execute(
        f'''
        SELECT form
        FROM form
        WHERE form IN (SELECT form FROM word
                       WHERE sentence_id = :sentence_id)
          AND form IN (SELECT form FROM learnable_form)
          AND form NOT IN (SELECT form FROM known_form)
        ORDER BY frequency DESC
        LIMIT 1;
        ''',
        {'sentence_id': sentence_id})
    return c.fetchone()[0]


def generate_new_potentials(db, form, limit):
    c = db.execute(
        '''
        SELECT sentence.id, word.form, text, translation FROM word
        JOIN sentence ON sentence.id = word.sentence_id
        JOIN sentence_stats ON sentence.id = sentence_stats.id
        WHERE word.form = ?
          AND sentence.id NOT IN (SELECT sentence_id FROM card
                                  WHERE word = ?)
        ORDER BY (word_count - known_words) ASC,
                 sentence_stats.length ASC
        LIMIT ?
        ''',
        (form, form, limit))

    return [{'sentence_id': r[0],
             'word': r[1],
             'text': r[2],
             'translation': r[3]}
            for r in c]


def enable_only_texts(db, enabled_ids):
    db.execute('BEGIN')

    db.execute('UPDATE text SET enabled = 0;')
    db.execute(
        f'''
        UPDATE text SET enabled = 1
        WHERE id IN ({', '.join(enabled_ids)});
        ''')

    db.commit()


def create_card(db, sentence_id, word):
    c = db.execute(
        '''
        SELECT word, punctuation, form FROM word
        WHERE sentence_id = ?
        ORDER BY id;
        ''',
        (sentence_id,))

    cloze_text = ' '.join(w[0] + w[1] if w[2] != word
                                      else '-----' + w[1]
                          for w in c)

    c = db.execute(
        '''
        SELECT text, translation FROM sentence
        WHERE id = ?;
        ''',
        (sentence_id,))
    sentence = c.fetchone()

    db.execute(
        '''
        INSERT INTO card (sentence_id, word, cloze_text, text, translation,
                          review_period_days, next_review)
        VALUES (?, ?, ?, ?, ?, 2, NULL)
        ''',
        (sentence_id,
         word,
         cloze_text,
         sentence[0],
         sentence[1]))

    db.commit()


def update_card_review(db, card, correct):
    db.execute('BEGIN')

    db.execute(
        '''
        INSERT INTO review (card_id, was_correct)
        VALUES (?, ?)
        ''',
        (card['id'], 'true' if correct else 'false'))

    c = db.execute(
        '''
        SELECT review_period_days FROM card
        WHERE id = ?;
        ''',
        (card['id'],))
    previous_review_period = c.fetchone()[0]

    if correct:
        new_review_period = max(previous_review_period + 1, round(previous_review_period * 1.5))
    else:
        new_review_period = round(previous_review_period * 0.7)

    # calculate a time in the early morning
    tz = pytz.timezone('Australia/Adelaide')
    now = datetime.now(tz)
    next_review = now + timedelta(days=previous_review_period)
    next_review = next_review.replace(hour=6, minute=0, second=0, microsecond=0)
    next_review = next_review.astimezone(pytz.utc)

    db.execute(
        '''
        UPDATE card
        SET review_period_days = ?,
            next_review = ?
        WHERE id = ?;
        ''',
        (new_review_period,
         next_review.isoformat(),
         card['id']))

    db.commit()


main()
