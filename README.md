# Grozer

A cloze-deletion flashcard player for New Testament Greek, inspired by
ClozeMaster.

## Setup

Download `OpenGNT_BASE_TEXT.zip` from [https://github.com/eliranwong/OpenGNT],
and extract `OpenGNT_version3_3.csv`.

Run `generate-db.py`, which will create the initial `main.db` summary of the
OpenGNT data.
