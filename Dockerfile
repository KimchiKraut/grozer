FROM python:3.8-slim
WORKDIR /app

COPY requirements.txt \
     /app/
RUN pip install --no-cache-dir -r requirements.txt

RUN groupadd --gid 10300 grozer \
 && useradd --uid 10300 grozer -g grozer


COPY *.py \
     *.js \
     *.css \
     *.md \
     /app/

USER grozer
VOLUME /var/grozer \
       /var/log
ENV DATA_PATH=/var/grozer \
    LOG_PATH=/var/log/grozer.log

EXPOSE 3000
CMD ["python", "/app/main.py"]
