;(function () {
    'use strict'

    const cardEl = document.querySelector('.card')
    const statsEl = cardEl.querySelector('.stats')
    const titleEl = cardEl.querySelector('.title')
    const contextLeftEl = cardEl.querySelector('.context .left')
    const contextRightEl = cardEl.querySelector('.context .right')
    const textEl = cardEl.querySelector('.context .text')
    const translationEl = cardEl.querySelector('.translation')
    const choice1El = cardEl.querySelector('.choices .c1')
    const choice2El = cardEl.querySelector('.choices .c2')
    const choice3El = cardEl.querySelector('.choices .c3')
    const choice4El = cardEl.querySelector('.choices .c4')
    const nextBtn = cardEl.querySelector('.next')
    const choiceEls = [choice1El, choice2El, choice3El, choice4El]

    let cards = window.grozerData.practice.slice()
    const total = cards.length
    let index = -1
    let correctCount = 0

    function init() {
        choiceEls.forEach(c => c.addEventListener('click', handleChoice))
        nextBtn.addEventListener('click', handleNext)
    }

    function current() {
        if (cards.length === 0 || index < 0) return null
        return cards[index]
    }

    function handleChoice(event) {
        const choice = event.target.textContent
        const card = current()

        const correct = choice === card.word

        if (correct) {
            correctCount++
        } else {
            cards.push(card)
        }

        if (!card.recorded) {
            fetch(`/api/card/${card.id}/${correct}`,
                  { method: 'POST' })
                .then(null,
                      e => console.warn('Failed to record result', e))
            card.recorded = true
        }

        renderStats()
        renderAnswer(card, correct)
    }

    function handleNext() {
        index++

        if (index < cards.length) {
            renderStats()
            renderCard(current())
        } else {
            // go to the dashboard
            window.location = '/'
        }
    }

    function renderStats() {
        const remaining = cards.length - index
        statsEl.textContent = `${total} cards; ${index} done; ${correctCount} correct; ${remaining} remaining`
    }

    function renderCard(card) {
        titleEl.textContent = 'Practice'

        contextLeftEl.textContent = card.left_context
        contextRightEl.textContent = card.right_context
        textEl.textContent = card.cloze_text
        translationEl.textContent = card.translation

        choiceEls.forEach((c, i) => { c.textContent = card.choices[i] })
        choiceEls.forEach(c => { c.disabled = false })
        nextBtn.style = 'display: none'
    }

    function renderAnswer(card, correct) {
        titleEl.textContent = correct ? 'Correct' : 'Incorrect'

        contextLeftEl.textContent = card.left_context
        contextRightEl.textContent = card.right_context
        textEl.textContent = card.text
        translationEl.textContent = card.translation

        choiceEls.forEach((c, i) => { c.textContent = card.choices[i] })
        choiceEls.forEach(c => { c.disabled = true })
        nextBtn.style = ''
    }

    init()
    handleNext()
})()
