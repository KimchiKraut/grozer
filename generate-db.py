import re
import sqlite3

from greek_normalisation.normalise import Normaliser, Norm
from greek_normalisation.utils import nfkc
import pandas as pd


normaliser = Normaliser(config=(
    Norm.GRAVE |
    Norm.ELISION |
    Norm.MOVABLE |
    Norm.EXTRA |
    Norm.PROCLITIC |
    Norm.ENCLITIC |
    Norm.NO_ACCENT
)).normalise

def normalise(text, lower=False):
    text, _ = normaliser(text)
    if lower:
        text = text.lower()
    return nfkc(text)


def open_csv():
    with open('OpenGNT_version3_3.csv') as fp:
        return pd.read_csv(
            fp,
            sep='\t',
            header=0,
            names=[
                'id',
                'id_tantt',
                'id_features',
                'clause_id',
                'ot_quotation',
                'sort_translation',
                'position',
                'word',
                'lexicons',
                'pronunciation',
                'translation',
                'punctuation',
                'misc',
            ],
            dtype=str,
        )

def clean_csv(frame):
    frame = frame[['id',
                   'clause_id',
                   'position',
                   'sort_translation',
                   'word',
                   'translation',
                   'punctuation',
            ]]

    frame = frame.assign(id=
                pd.to_numeric(frame['id']))
    frame = frame.assign(clause_id=
                pd.to_numeric(frame['clause_id'].map(lambda s: s[1:])))

    position = frame['position'].map(lambda s: s[1:-1]).str.split('｜', expand=True)
    position = position.rename(columns={0:'book', 1:'chapter', 2:'verse'})
    frame = frame.drop('position', axis=1)
    frame = frame.assign(
                book=position['book'],
                chapter=position['chapter'],
                verse=position['verse'],
            )

    sort_translation = frame['sort_translation'].map(lambda s: s[1:-1]).str.split('｜', expand=True)
    sort_translation = sort_translation.rename(columns={0:'bgb', 1:'literal', 2:'study'})
    not_numbers = re.compile('[^0-9]+')
    frame = frame.drop('sort_translation', axis=1)
    frame = frame.assign(
                sort_study=pd.to_numeric(sort_translation['study'].str.replace(not_numbers, '')),
            )

    word = frame['word'].map(lambda s: s[1:-1]).str.split('｜', expand=True)
    word = word.rename(columns={0:'gk', 1:'gu', 2:'ga', 3:'gl', 4:'strongs', 5:'rmac'})
    frame = frame.drop('word', axis=1)
    frame = frame.assign(
                word=word['ga'].map(normalise),
                form=word['ga'].map(lambda w: normalise(w, lower=True)),
                lexeme=word['gl'].map(normalise),
            )

    translation = frame['translation'].map(lambda s: s[1:-1]).str.split('｜', expand=True)
    translation = translation.rename(columns={0:'tbesg', 1:'inter', 2:'literal', 3:'study', 4:'spanish'})
    frame = frame.drop('translation', axis=1)
    frame = frame.assign(
                interlinear=translation['inter'],
                study=translation['study'],
            )

    punctuation = frame['punctuation'].map(lambda s: s[1:-1]).str.split('｜', expand=True)
    punctuation = punctuation.rename(columns={0:'start', 1:'end'})
    pend = punctuation['end'].str.replace(re.compile(r'\[|\]|</?pm>'), '')
    pend = pend.str.replace('¶', '')
    pend = pend.str.replace(';', ';')
    pend = pend.str.replace(';,', ';')
    pend = pend.str.replace(';—', ';')
    pend = pend.str.replace(',—', '—')
    frame = frame.drop('punctuation', axis=1)
    frame = frame.assign(
                sentence=punctuation['end'].str.contains(r'[;.¶]', regex=True),
                paragraph=punctuation['end'].str.contains('¶', regex=False),
                punctuation=pend,
            )

    return frame

def chunk_into_sentences(words_frame):
    min_valid_text = re.compile('[a-zA-Z:()]')
    def valid_word(text):
        return min_valid_text.search(text) and 'vvv' not in text and '...' not in text

    sentences = []

    sentence = None
    paragraph_counter = 0
    sentence_counter = 0
    for i, row in words_frame.iterrows():
        if sentence is None:
            sentence = {
                'id': sentence_counter,
                'paragraph': paragraph_counter,
                'start_word': row['id'],
                'end_word': row['id'],
                'book': row['book'],
                'chapter': row['chapter'],
                'verse': row['verse'],
                'text': [],
                'translation': [],
            }
            sentences.append(sentence)

        sentence['end_word'] = row['id']
        sentence['text'].append(row['word'] + row['punctuation'])
        sentence['translation'].append((row['sort_study'], row['study']))

        if row['paragraph']:
            paragraph_counter += 1

        if row['sentence']:
            sentence['text'] = ' '.join(sentence['text'])

            sentence['translation'].sort(key=lambda t: t[0])
            sentence['translation'] = [t[1] for t in sentence['translation']]
            sentence['translation'] = [t for t in sentence['translation']
                                       if valid_word(t)]
            sentence['translation'] = ' '.join(sentence['translation'])

            sentence = None
            sentence_counter += 1

    if sentence:
        raise Exception('Data did not finish the last sentence.')

    return pd.DataFrame(sentences)


def write_sqlite(words, sentences):
    with sqlite3.connect('main.db') as db:
        db.execute('''DROP TABLE IF EXISTS sentence;''')
        db.execute('''DROP TABLE IF EXISTS word;''')
        db.execute('''DROP TABLE IF EXISTS form;''')

        db.execute(
            '''
            CREATE TABLE word (
                id INT PRIMARY KEY,
                clause_id INT,
                text_id INT,
                book INT,
                chapter INT,
                verse INT,
                word TEXT,
                form TEXT,
                lexeme TEXT,
                interlinear TEXT,
                study TEXT,
                sentence_id INT,
                sentence BOOLEAN,
                paragraph BOOLEAN,
                punctuation TEXT
            );
            ''')

        db.execute(
            '''
            CREATE TABLE sentence (
                id INT PRIMARY KEY,
                text_id INT,
                paragraph INT,
                start_word INT,
                end_word INT,
                book INT,
                chapter INT,
                verse INT,
                text TEXT,
                translation TEXT
            );
            ''')

        db.execute(
            '''
            CREATE TABLE form (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                form TEXT UNIQUE,
                lexeme TEXT,
                frequency INT
            );
            ''')

        db.execute(
            '''
            CREATE TABLE text (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                title TEXT UNIQUE,
                enabled BOOLEAN
            );
            ''')

        print('Inserting words...')
        word_rows = []
        for i, row in words.iterrows():
            word_rows.append((
                row['id'], row['clause_id'], row['book'], row['chapter'],
                row['verse'], row['word'], row['form'], row['lexeme'],
                row['interlinear'], row['study'], row['sentence'],
                row['paragraph'], row['punctuation'],
            ))
        db.executemany(
            '''
            INSERT INTO word (id, clause_id, book, chapter, verse, word, form,
                              lexeme, interlinear, study, sentence,
                              paragraph, punctuation)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            ''',
            word_rows)

        print('Inserting sentences...')
        sentence_rows = []
        for i, row in sentences.iterrows():
            sentence_rows.append((
                row['id'], row['paragraph'], row['start_word'], row['end_word'],
                row['book'], row['chapter'], row['verse'], row['text'],
                row['translation'],
            ))
        db.executemany(
            '''
            INSERT INTO sentence (id, paragraph, start_word, end_word, book,
                                  chapter, verse, text, translation)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);
            ''',
            sentence_rows)

        print('Creating indexes...')
        db.execute('''CREATE INDEX word_word ON word (word);''')
        db.execute('''CREATE INDEX word_form ON word (form);''')
        db.execute('''CREATE INDEX sentence_start_word ON sentence (start_word);''')
        db.execute('''CREATE INDEX sentence_end_word ON sentence (end_word);''')

        db.execute(
            '''
            UPDATE word
            SET sentence_id = (SELECT sentence.id
                               FROM sentence
                               WHERE start_word <= word.id
                                 AND end_word >= word.id);
            ''')

        print('Counting forms...')
        db.execute(
            '''
            INSERT INTO form (form, lexeme, frequency)
            SELECT form, lexeme, COUNT(*) AS count
            FROM word
            GROUP BY form
            ORDER BY count DESC;
            ''')

        db.execute('''CREATE INDEX form_frequency ON form (frequency);''')


        print('Indexing chapters as texts...')
        db.execute('CREATE TABLE book (id INT PRIMARY KEY, name TEXT);')

        db.execute(
            '''
            INSERT INTO book (id, name)
            VALUES (40, 'Matthew'),
                   (41, 'Mark'),
                   (42, 'Luke'),
                   (43, 'John'),
                   (44, 'Acts'),
                   (45, 'Romans'),
                   (46, '1 Corinthians'),
                   (47, '2 Corinthians'),
                   (48, 'Galatians'),
                   (49, 'Ephesians'),
                   (50, 'Philippians'),
                   (51, 'Colossians'),
                   (52, '1 Thessalonians'),
                   (53, '2 Thessalonians'),
                   (54, '1 Timothy'),
                   (55, '2 Timothy'),
                   (56, 'Titus'),
                   (57, 'Philemon'),
                   (58, 'Hebrews'),
                   (59, 'James'),
                   (60, '1 Peter'),
                   (61, '2 Peter'),
                   (62, '1 John'),
                   (63, '2 John'),
                   (64, '3 John'),
                   (65, 'Jude'),
                   (66, 'Revelation');
            ''')

        db.execute(
            '''
            INSERT INTO text (id, title)
            SELECT book * 100 + chapter,
                   'GNT ' || book.name || ' ' || chapter
            FROM word
            JOIN book ON word.book = book.id
            GROUP BY book, chapter;
            ''')
        db.execute('UPDATE text SET enabled = 0;')

        db.execute(
            '''
            UPDATE word SET text_id = (
                SELECT text.id FROM text
                WHERE text.id = word.book * 100 + word.chapter);
            ''')

        db.execute(
            '''
            UPDATE sentence SET text_id = (
                SELECT text.id FROM text
                WHERE text.id = sentence.book * 100 + sentence.chapter);
            ''')

        db.execute('DROP TABLE book;')


        db.commit()


print('Loading...')
frame = open_csv()

print('Cleaning...')
frame = clean_csv(frame)

print('Chunking into sentences...')
sentences = chunk_into_sentences(frame)

print('Writing SQLite database...')
write_sqlite(frame, sentences)
